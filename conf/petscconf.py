# --------------------------------------------------------------------

__all__ = ['setup',
           'Extension',
           'config',
           'build',
           'build_src',
           'build_ext',
           'test',
           'sdist',
           ]

# --------------------------------------------------------------------

from conf.baseconf import setup, Extension
from conf.baseconf import config, build, build_src, build_ext
from conf.baseconf import test, sdist

# --------------------------------------------------------------------
